require 'sinatra/base'
require 'sinatra/config_file'
require 'sinatra/flash'
require 'rest-client'
require 'json'

require_relative 'lib/payway_client'

####################################################
# Adyen Payway checkout code example
# This is by no means a complete tutorial on how you integrate Payway - Adyen checkout into your site. It is only meant to be a guide.
# Before you proceed, please fill out settings/config.yml
# ####################################

class App < Sinatra::Base

  enable :sessions
  register Sinatra::ConfigFile
  register Sinatra::Flash

  use Rack::Session::Cookie,
      :key => 'rack.session',
      :domain => 'localhost',
      :path => '/',
      :expire_after => 2592000,
      :secret => '[fill_in_your_own_secret]',
      :old_secret => '[fill_in_your_own_old_secret]'

  config_file 'settings/config.yml'

  ####################################################
  # First page of article teasers
  # ####################################
  get '/' do
    erb :index, :layout => :layout, :locals => { :identity => session[:access_token] }
  end

  ####################################################
  # Render article teaser and purchase flow
  # Check access
  # ####################################
  get '/teaser' do
    if session[:access_token]
      begin
        if active_products.include?(settings.product_code)
          redirect '/article'
        else
          configuration_settings = get_configuration_settings(settings.product_code)
          payment_methods = get_payment_methods(settings.product_code, settings.period_id)
          model = {
            :payment_methods => payment_methods,
            :origin_key => configuration_settings[:origin_key],
            :client_key => configuration_settings[:client_key],
            :locale => configuration_settings[:locale],
            :javascript_uri => configuration_settings[:javascript_uri],
            :javascript_uri_integrity => configuration_settings[:javascript_uri_integrity],
            :css_uri => configuration_settings[:css_uri],
            :css_uri_integrity => configuration_settings[:css_uri_integrity]
          }
          erb :teaser, :layout => :layout, :locals => model
        end
      rescue OAuthError => e
        create_access_token_from_refresh_token
        redirect '/article'
      rescue StandardError => e
        flash[:error] = e.message
        redirect '/error'
      end
    else
      redirect '/login'
    end
  end


  ####################################################
  # Render article teaser and purchase flow
  # Check access
  # ####################################
  get '/gift' do
    if session[:access_token]
      begin
          configuration_settings = get_configuration_settings(settings.product_code)
          payment_methods = get_payment_methods(settings.product_code, settings.period_id)
          model = {
            :payment_methods => payment_methods,
            :origin_key => configuration_settings[:origin_key],
            :client_key => configuration_settings[:client_key],
            :locale => configuration_settings[:locale],
            :javascript_uri => configuration_settings[:javascript_uri],
            :javascript_uri_integrity => configuration_settings[:javascript_uri_integrity],
            :css_uri => configuration_settings[:css_uri],
            :css_uri_integrity => configuration_settings[:css_uri_integrity]
          }
          erb :gift, :layout => :layout, :locals => model

      rescue OAuthError => e
        create_access_token_from_refresh_token
        redirect '/gift'
      rescue StandardError => e
        flash[:error] = e.message
        redirect '/error'
      end
    else
      redirect '/login'
    end
  end

  ####################################################
  # Purchase page, user presses "purchase"
  # ####################################
  post '/gift' do
    begin
      browser_info = build_browser_info(request, params)
      payment_method = params[:stateData][:paymentMethod]
      return_url = 'http://localhost:9292/callback'
      delivery_address = {
          :first_name => 'Eskil',
          :last_name => 'Svensson',
          :street => 'Vägen',
          :street_number => '173',
          :apartment => 'lgh',
          :apartment_number => '3',
          :city => 'Stockholm',
          :country_code => 'SE',
          :zip_code => '10316'
      }
      response = place_gift_order(payment_method, settings.product_code, settings.period_id, return_url, browser_info, nil, delivery_address )
      session[:session_reference] = response[:session_reference] if response[:session_reference]
      response.to_json
    rescue OAuthError => e
      create_access_token_from_refresh_token
      redirect '/article'
    rescue StandardError => e
      flash[:error] = e.message
      {:message => e.message}.to_json
    end
  end

  ####################################################
  # Purchase page, user presses "purchase"
  # ####################################
  post '/teaser' do
    begin
      browser_info = build_browser_info(request, params)
      payment_method = params[:stateData][:paymentMethod]
      return_url = 'http://localhost:9292/callback'
      response = place_order(payment_method, settings.product_code, settings.period_id, return_url, browser_info, nil, params[:voucherPurchase])
      session[:session_reference] = response[:session_reference] if response[:session_reference]
      response.to_json
    rescue OAuthError => e
      create_access_token_from_refresh_token
      redirect '/article'
    rescue StandardError => e
      flash[:error] = e.message
      {:message => e.message}.to_json
    end
  end

  ####################################################
  # Render article teaser for purchase flow without identity
  # ####################################
  get '/purchase_without_account' do
    begin
      create_access_token_without_identity
      configuration_settings = get_configuration_settings(settings.product_code)
      payment_methods = get_payment_methods(settings.product_code, settings.period_id)
      erb :purchase_without_account, :layout => :layout, :locals => {
        :payment_methods => payment_methods,
        :origin_key => configuration_settings[:origin_key],
        :client_key => configuration_settings[:client_key],
        :locale => configuration_settings[:locale],
        :javascript_uri => configuration_settings[:javascript_uri],
        :javascript_uri_integrity => configuration_settings[:javascript_uri_integrity],
        :css_uri => configuration_settings[:css_uri],
        :css_uri_integrity => configuration_settings[:css_uri_integrity],
      }
    rescue StandardError => e
      flash[:error] = e.message
      redirect '/error'
    end
  end

  ####################################################
  # Purchase page, user presses "purchase"
  # ####################################
  post '/purchase_without_account' do
    puts "email #{params[:email]}"
    if params[:email]
      begin
        browser_info = build_browser_info(request, params)
        payment_method = params[:stateData][:paymentMethod]
        return_url = 'http://localhost:9292/callback'
        response = place_order_without_account(payment_method, settings.product_code, settings.period_id, return_url, browser_info, nil, false, params[:email])
        session[:session_reference] = response[:session_reference] if response[:session_reference]
        response.to_json
      rescue StandardError => e
        flash[:error] = e.message
        {:message => e.message}.to_json
      end
    else
      flash[:error] = 'Email address required for purchase without identity'
      redirect '/error'
    end
  end
  ####################################################
  # Purchase_successful for purchase_without_account
  # ####################################
  get '/purchase_successful' do
    begin
      erb :purchase_successful, :layout => :layout, :locals => {}
    rescue StandardError => e
      log_error(e)
      flash[:error] = e.message
      redirect '/error'
    end
  end

  ####################################################
  # Complete article, check access
  # ####################################
  get '/article' do
    begin
      if session[:voucher_token]
        redirect '/voucher_success'
      elsif session[:access_token]
        if active_products.include?(settings.product_code)
          pending = params[:pending].nil? ? false : params[:pending]
          erb :article, :layout => :layout, :locals => {:pending => pending}
        else
          redirect '/teaser'
        end
      else
        redirect '/login'
      end
    rescue OAuthError => e
      create_access_token_from_refresh_token
      redirect '/article'
    rescue StandardError => e
      log_error(e)
      flash[:error] = e.message
      redirect '/error'
    end
  end

  get '/voucher_success' do
    erb :voucher_success, :layout => :layout
  end

  ####################################################
  # Perform additional action. This method is called when a user is redirected back from e.q. third party verification.
  # You define this route as the "return_url" in the first /place_order request.
  # This route also needs to be defined as a HTTP POST.
  # ####################################
  get '/callback' do
    begin
      details = params # Here we get all input parameters from the query string
      session_reference = session[:session_reference] # Pick up your session_reference received in the first /place_order response
      response = perform_additional_action(details, session_reference)
      case response[:status]
        when 'complete'
          redirect '/article'
        when 'pending'
          redirect '/article?pending=true'
        else
          redirect '/error'
      end
    rescue StandardError => e
      log_error(e)
      flash[:error] = e.message
      redirect '/error'
    end
  end

  ####################################################
  # Perform additional action. This method is called when a user is redirected back from e.q. third party verification.
  # You define this route as the "return_url" in the first /place_order request.
  # # This route also needs to be defined as a HTTP GET.
  # ####################################
  post '/callback' do
    begin
      details = params # Here we get all input parameters from the query string
      session_reference = session[:session_reference] # Pick up your session_reference received in the first /place_order response
      response = perform_additional_action(details, session_reference)
      case response[:status]
        when 'complete'
          redirect '/article'
        when 'pending'
          redirect '/article?pending=true'
        else
          redirect '/error'
      end
    rescue StandardError => e
      log_error(e)
      flash[:error] = e.message
      redirect '/error'
    end
  end

  ####################################################
  # Perform additional action. This method should be called when a user is required to perform additional actions from the Adyen widget.
  # ####################################
  post '/additional_action' do
    begin
      details = params # Here we get all input parameters from the form body
      session_reference = session[:session_reference] # Pick up your session_reference received in the first /place_order response
      response = perform_additional_action(details, session_reference)
      response.to_json
    rescue OAuthError => e
      create_access_token_from_refresh_token
      redirect '/article'
    rescue StandardError => e
      log_error(e)
      flash[:error] = e.message
      {:message => e.message}.to_json
    end
  end

  ####################################################
  # Login page. Does not need to be a separate page. But you need an access_token with an identity to complete a purchase.
  # ####################################
  get '/login' do
    erb :login, :layout => :layout
  end

  ####################################################
  # Login page. Does not need to be a separate page. But you need an access_token with an identity to complete a purchase.
  # ####################################
  post '/login' do
    user = params[:user]
    pwd = params[:pwd]
    begin
      create_access_token(user, pwd)
      redirect '/teaser'
    rescue StandardError => e
      log_error(e)
      flash[:error] = e.message
      redirect '/login'
    end
  end

  get '/verify_card_cancel' do
    erb :verify_card_cancel, :layout => layout
  end

  get '/verify_card' do
    if session[:access_token]
      begin
        raise StandardError.new('associated_order_id required for verify') if settings.associated_order_id.nil? or settings.associated_order_id.empty?
        configuration_settings = get_configuration_settings(settings.product_code)
        payment_methods = get_payment_methods(settings.product_code, settings.period_id)
        model = {
          :payment_methods => payment_methods,
          :origin_key => configuration_settings[:origin_key],
          :client_key => configuration_settings[:client_key],
          :locale => configuration_settings[:locale],
          :javascript_uri => configuration_settings[:javascript_uri],
          :javascript_uri_integrity => configuration_settings[:javascript_uri_integrity],
          :css_uri => configuration_settings[:css_uri],
          :css_uri_integrity => configuration_settings[:css_uri_integrity]
        }
        erb :verify_card, :layout => :layout, :locals => model
      rescue OAuthError => e
        create_access_token_from_refresh_token
        redirect '/error'
      rescue StandardError => e
        flash[:error] = e.message
        redirect '/error'
      end
    else
      redirect '/login'
    end
  end

  post '/verify_card' do
    begin
      browser_info = build_browser_info(request, params)
      payment_method = params[:stateData][:paymentMethod]
      order_id = settings.associated_order_id
      return_url = "#{request.scheme}://#{request.host}"
      response = perform_verify_card(browser_info, order_id, payment_method, return_url)
      session[:session_reference] = response[:session_reference] if response[:session_reference]
      response.to_json
    rescue OAuthError => e
      create_access_token_from_refresh_token
      redirect '/error'
    rescue StandardError => e
      flash[:error] = e.message
      redirect '/error'
    end
  end

  ####################################################
  # Something went wrong
  # ####################################
  get '/error' do
    message = flash[:message] ? flash[:message] : params[:message]
    erb :error, :layout => :layout, :locals => { :message => message }
  end

  ####################################################
  # Create access_token with identity using password grant. Not a required way of implementing.
  # Read more on acquiring access token with identity.
  # https://docs.worldoftulo.com/payway/integration/payway_api/api_access/#access-token
  # ####################################
  def create_access_token(user, pwd)
    payload = {
        'grant_type': 'password',
        'client_id': settings.client_id,
        'client_secret': settings.client_secret,
        'scope': settings.scopes,
        'username': "#{settings.organisation_id}^#{user}",
        'password': pwd
    }
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json'
    }
    response = PaywayClient.post(settings.access_token_uri, payload, headers)
    session[:access_token] = response[:access_token]
    session[:refresh_token] = response[:refresh_token] if response[:refresh_token]
  end

  ####################################################
  # Create access_token without identity using none grant. Not a required way of implementing.
  # Read more on acquiring access token with identity.
  # https://docs.worldoftulo.com/payway/integration/payway_api/api_access/#access-token
  # ####################################
  def create_access_token_without_identity
    payload = {
      'grant_type': 'none',
      'client_id': settings.client_id,
      'client_secret': settings.client_secret,
      'scope': settings.scopes
    }
    headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json'
    }
    response = PaywayClient.post(settings.access_token_uri, payload, headers)
    puts response[:access_token]
    session[:access_token] = response[:access_token]
  end

  ####################################################
  # Acquire new access_token from refresh token
  # Read more on acquiring access token with refresh token
  # https://docs.worldoftulo.com/payway/integration/payway_api/api_access/#handling-access-token-expiry-with-refresh-tokens
  # ####################################
  def create_access_token_from_refresh_token
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json'
    }
    payload = {
        'grant_type': 'refresh_token',
        'refresh_token': session[:refresh_token],
        'client_id': settings.client_id,
        'client_secret': settings.client_secret,
        'redirect_uri': 'http://localhost:9292'
    }
    response = PaywayClient.post(settings.access_token_uri, payload, headers)
    session[:access_token] = response[:access_token]
    session[:refresh_token] = response[:refresh_token] if response[:refresh_token]
  end

  ####################################################
  # Fetches available payment methods.
  # ####################################
  def get_payment_methods(product_code,period_id)
    headers = {
        'Authorization' => "Bearer #{session[:access_token]}",
        'Accept' => 'application/json'
    }
    origin = "#{request.scheme}://#{request.host}"
    url = "#{settings.api_url_payment_methods}?product_code=#{product_code}&period_id=#{period_id}&origin=#{origin}"
    "{\"paymentMethods\":#{PaywayClient.get(url, headers)[:items].to_json}}"
  end

  ####################################################
  # Verify a credit card against our own external API (usually on behalf of a user)
  # ####################################
  def perform_verify_card(browser_info, order_id, payment_method, return_url)
    headers = {
      'Authorization' => "Bearer #{session[:access_token]}",
      'Content-Type' => 'application/json',
      'Accept' => 'application/json'
    }
    payload = {
      :order_id => order_id,
      :browser_info => browser_info,
      :payment_method => payment_method,
      :origin => "#{request.scheme}://#{request.host}",
      :return_url => return_url
    }
    url = "#{settings.api_url_verify_card}"
    PaywayClient.post(url, payload.to_json, headers)[:item]
  end

  ####################################################
  # Fetches settings.
  # ####################################
  def get_configuration_settings(product_code)
    headers = {
        'Authorization' => "Bearer #{session[:access_token]}",
        'Accept' => 'application/json'
    }
    origin = "#{request.scheme}://#{request.host}"
    url = "#{settings.api_url_settings}?product_code=#{product_code}&origin=#{origin}"
    PaywayClient.get(url, headers)[:item]
  end

  ####################################################
  # Place the order. Depending on the response you might need to perform additional actions.
  # ####################################
  def place_order(payment_method, product_code, period_id, return_url, browser_info, traffic_source, voucher_purchase)
    headers = {
        'Authorization' => "Bearer #{session[:access_token]}",
        'Content-Type' => 'application/json',
        'Accept' => 'application/json'
    }
    payload = {
        :payment_method => payment_method,
        :product_code => product_code,
        :period_id => period_id,
        :return_url => return_url,
        :browser_info => browser_info,
        :traffic_source => traffic_source,
        :origin => "#{request.scheme}://#{request.host}"
    }
    voucher_purchase = voucher_purchase.to_s.downcase == "true" ? true : false
    if voucher_purchase
      voucher_payload = {
        :voucher_purchase => true,
        :voucher_details => {
          :expire_date => (DateTime.now + 30).to_s
        }
      }
      payload.merge!(voucher_payload)
      session[:voucher_token] = true
    end

    url = "#{settings.api_url_adyen_place_order}"
    PaywayClient.post(url, payload.to_json, headers)[:item]
  end

  ####################################################
  # Place the gift order. Depending on the response you might need to perform additional actions.
  # ####################################
  def place_gift_order(payment_method, product_code, period_id, return_url, browser_info, traffic_source, delivery_address)
    headers = {
      'Authorization' => "Bearer #{session[:access_token]}",
      'Content-Type' => 'application/json',
      'Accept' => 'application/json'
    }
    payload = {
      :payment_method => payment_method,
      :product_code => product_code,
      :period_id => period_id,
      :return_url => return_url,
      :browser_info => browser_info,
      :traffic_source => traffic_source,
      :origin => "#{request.scheme}://#{request.host}",
      :delivery_address => delivery_address
    }

    url = "#{settings.api_url_adyen_place_gift_order}"
    PaywayClient.post(url, payload.to_json, headers)[:item]
  end

  ####################################################
  # Place the order without account. Depending on the response you might need to perform additional actions.
  # ####################################
  def place_order_without_account(payment_method, product_code, period_id, return_url, browser_info, traffic_source, voucher_purchase, email)
    headers = {
      'Authorization' => "Bearer #{session[:access_token]}",
      'Content-Type' => 'application/json',
      'Accept' => 'application/json'
    }
    payload = {
      :payment_method => payment_method,
      :product_code => product_code,
      :period_id => period_id,
      :return_url => return_url,
      :browser_info => browser_info,
      :traffic_source => traffic_source,
      :origin => "#{request.scheme}://#{request.host}",
      :account => {
        :email => email,
        :first_name => "Test",
        :last_name => 'Testsson'
      }
    }
    voucher_purchase = voucher_purchase.to_s.downcase == "true" ? true : false
    if voucher_purchase
      payload.merge!({
                       :voucher_purchase => true,
                       :voucher_details => {
                         :access_days => 30,
                         :expire_date => (DateTime.now + 30).to_s
                       }
                     })
      session[:voucher_token] = true
    end

    url = "#{settings.api_url_adyen_place_order}"
    PaywayClient.post(url, payload.to_json, headers)[:item]
  end

  ####################################################
  # Perform additional action required by payment method.
  # ####################################
  def perform_additional_action(details, session_reference)
    headers = {
        'Authorization' => "Bearer #{session[:access_token]}",
        'Content-Type' => 'application/json',
        'Accept' => 'application/json'
    }
    payload = {
        :session_reference => session_reference,
        :details => details,
        :origin => "#{request.scheme}://#{request.host}"
    }
    url = "#{settings.api_url_perform_additional_action}"
    PaywayClient.post(url, payload.to_json, headers)[:item]
  end

  ####################################################
  # Fetch active products for current user to check access
  # ####################################
  def active_products
      headers = {
          'Authorization' => "Bearer #{session[:access_token]}",
          'Accept' => 'application/json'
      }
      url = "#{settings.api_me_active_products}"
      PaywayClient.get(url, headers)[:item][:active_products]
  end

  def build_browser_info(request, client)
    {
        :browser_ip => request.ip,
        :browser_language => request.env['HTTP_ACCEPT_LANGUAGE'].split(',')[0],
        :browser_user_agent => request.user_agent,
        :referer => request.env['HTTP_REFERER'],
        :accept_header => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        :color_depth => client[:colorDepth],
        :screen_height => client[:screenHeight],
        :screen_width => client[:screenWidth],
        :time_zone_offset => client[:timeZoneOffset],
        :java_enabled => client[:javaEnabled]
    }
  end

  def log_error(e)
    puts e.message
    puts e.backtrace.join("\n")
  end
end
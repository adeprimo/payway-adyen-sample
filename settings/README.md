# Tulo Adyen Sample

Sample application that shows how to embed Adyen through Payway to enable customers to purchase directly on the newspaper site. 
This sample features a use case where you acquire an access token with an identity using the password grant.
The access token is then used to complete the purchase and check access using the Payway API.

This is a ruby application which requires that you have ruby interpreter along with "bundler" installed on your system.

### This is by no means a complete tutorial on how you integrate Payway - Adyen checkout into your site. It is only meant to be a guide.

## Configuration

Create if not exists settings/config.yml and edit the configuration and insert valid values for:

```
development:
  #The client id of your api user. Found in PAP.
  client_id: 
  #The client secret of your api user. Found in PAP. Store this value with great care.
  client_secret: 
  #Scopes required for example. Might differ for you. But adyen r/w is required to complete a purchase.
  scopes: 

  #Your organisational identifier in Payway. Found in PAP.
  organisation_id: 
  #The product code in payway you want to sell. Found in PAP.
  product_code: 
  #The purchase period to complete the purchase with. Found in PAP.
  #Or you can use the Payway API to fetch info on payment periods.
  period_id: 

  # Associated order id. Should be a Adyen-order, in the format: ORGANISATION-1
  associated_order_id: 

  #Payway API url's used in example
  access_token_uri: 
  api_url_settings: 
  api_url_payment_methods: 
  api_url_place_order: 
  api_url_perform_additional_action: 
  api_url_verify_card: 
  api_me_active_products: 

You need to setup an API user with at least _/external/me/w /external/adyen/r /external/adyen/w_ to complete the sample purchase.
Other scopes might be needed depending on your implementation.
```

## Starting the application

In the directory where you have this application, first install dependencies:
```
bundle install
```

Then start the application:
```
bundle exec rackup
```

Open the application in your browser => [http://localhost:9292](http://localhost:9292)

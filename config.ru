####################################################
# Adyen Payway checkout code example
# ####################################

require './app'
require 'openssl'

use Rack::Static, :urls => ["/public"]

#OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE

map '/' do
  run App
end
